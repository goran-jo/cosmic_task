<?php

use App\Api\V1\Controllers\CosmicController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => env('APP_ENV'),
    'namespace' => env('API_VERSION') . '\Controllers', 'prefix' => env('API_VERSION')], function () {
    Route::get('connect/', [CosmicController::class, 'index']);
    Route::get('users/', [CosmicController::class, 'search'])->middleware('bearerable');
});
