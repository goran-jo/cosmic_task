<?php

use App\Http\Controllers\CosmicController;
use Illuminate\Support\Facades\Route;

Route::get('/cosmic', [CosmicController::class, 'index']);
Route::get('/cosmic/users', [CosmicController::class, 'users'])->name('list')->middleware('bearerable');
