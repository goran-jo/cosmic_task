<?php

namespace App\Http\Controllers;

use App\Api\Client\Cosmic\CosmicApiClient;
use App\Api\Client\Cosmic\Entity\GetUsers;
use App\Api\Client\Cosmic\Entity\PasswordAuthentication;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CosmicController extends Controller
{
    private $cosmicApiClient;
    private $authentication;
    private $getUsers;

    public function __construct(CosmicApiClient $cosmicApiClient, PasswordAuthentication $authentication, GetUsers $getUsers)
    {
        $this->cosmicApiClient = $cosmicApiClient;
        $this->authentication = $authentication;
        $this->getUsers = $getUsers;
    }

    public function index()
    {
        $prepareUrl = $this->cosmicApiClient->connect($this->authentication);

        try {
            $response = Http::post($prepareUrl[0], $prepareUrl[1]);
        } catch (NotFoundHttpException $exception) {
            return $exception->getMessage();
        }

        $response = $this->transformResponseData($response->body());

        if ($response && $response['status'] === 'success') {
//            dd($response);
//            Cache::add('bearer', $response['data']['access_token'], $response['data']['expires_at']);
            Cache::add('bearer', $response['data']['access_token']);
        }

        return Response::json(['success' => 'yeah, logged in']);

    }

    /**
     * @return JsonResponse
     * @throws \Exception
     */
    public function users(): JsonResponse
    {
        $bearer = Cache::get('bearer');
        $prepareUrl = $this->cosmicApiClient->getUsers($this->getUsers, $bearer);

        $response = Http::withHeaders($prepareUrl[1])->send('GET', $prepareUrl[0]);
        $response = $this->transformResponseData($response->body());


        return Response::json($response);
    }


    /**
     * @param $response
     * @return mixed
     */
    protected function transformResponseData($response)
    {
        return json_decode($response, true);
    }
}
