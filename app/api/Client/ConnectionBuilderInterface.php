<?php

namespace App\Api\Client;

interface ConnectionBuilderInterface
{
    public function clientId(string $clientID);
    public function clientSecret(string $clientSecret);
}
