<?php

namespace App\Api\Client;

interface AuthorizationBearer
{
    public function bearer(string $bearer);
}
