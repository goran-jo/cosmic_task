<?php

namespace App\Api\Client\Grant;

interface GrantTypePasswordInterface
{
    public function grantType(string $type);

    public function username(string $username);

    public function password(string $password);
}
