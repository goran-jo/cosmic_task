<?php

namespace App\Api\Client\Cosmic;

use App\Api\Client\Cosmic\Entity\GetUsers;
use App\Api\Client\Cosmic\Entity\PasswordAuthentication;
use Illuminate\Support\Facades\Cache;

class CosmicApiClient extends ClientAbstract
{
    /**
     * @return string[]
     */
    protected function endpoints(): array
    {
        return [
            'post.connect' => 'http://technical_test.client.cosmicdevelopment.com/api/token/',
            'get.users' => 'http://technical_test.client.cosmicdevelopment.com/api/employee/list/'
        ];
    }

    /**
     * @param $key
     * @return string
     */
    private function getEndpoint($key): string
    {
        return $this->endpoints()[$key];
    }

    /**
     * @param PasswordAuthentication $authentication
     * @return array
     */
    public function connect(PasswordAuthentication $authentication): array
    {
        $params = $authentication
            ->grantType('password')
            ->clientId(env('CONNECT_CLIENT_ID'))
            ->clientSecret(env('CONNECT_CLIENT_SECRET'))
            ->username(env('CONNECT_USERNAME'))
            ->password(env('CONNECT_PASSWORD'))
            ->setBuilder();

        return [$this->getEndpoint('post.connect'), $params];
    }

    /**
     * @param GetUsers $users
     * @param $bearer
     * @return array
     */
    public function getUsers(GetUsers $users, $bearer): array
    {
        $params = $users
            ->bearer($bearer)
            ->setBuilder();

//        return $this->urlQuery($this->getEndpoint('get.users'), $params);
        return [$this->getEndpoint('get.users'), $params];
    }
}
