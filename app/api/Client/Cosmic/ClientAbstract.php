<?php

namespace App\Api\Client\Cosmic;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

abstract class ClientAbstract
{
    /**
     * @param $to
     * @param array $params
     * @param array $additional
     * @return string
     */
    protected function urlQuery($to, array $params = [], array $additional = []): string
    {
        return Str::finish(url($to, $additional), '?') . Arr::query($params);
    }

    protected abstract function endpoints();
}
