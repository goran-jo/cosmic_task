<?php

namespace App\Api\Client\Cosmic\Entity;

use App\Api\Client\AuthorizationBearer;

class GetUsers implements AuthorizationBearer
{
    private array $builder;

    /**
     * @param string $bearer
     * @return GetUsers
     */
    public function bearer(string $bearer): GetUsers
    {
        $this->builder['Access-Token'] = $bearer;

        return $this;
    }

    /**
     * @return array
     */
    public function setBuilder(): array
    {
        return $this->builder;
    }
}
