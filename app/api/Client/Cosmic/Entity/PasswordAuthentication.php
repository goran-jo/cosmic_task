<?php

namespace App\Api\Client\Cosmic\Entity;

use App\Api\Client\ConnectionBuilderInterface;
use App\Api\Client\Grant\GrantTypePasswordInterface;

class PasswordAuthentication implements ConnectionBuilderInterface, GrantTypePasswordInterface
{
    private array $builder;

//"grant_type" : "password",
//"client_id": "6779ef20e75817b79601",
//"client_secret": "3e0f85f44b9ffbc87e90acf40d482601",
//"username": "hiring",
//"password": "cosmicdev"

    /**
     * @param string $type
     * @return PasswordAuthentication
     */
    public function grantType(string $type): PasswordAuthentication
    {
        $this->builder['grant_type'] = $type;

        return $this;
    }

    /**
     * @param string $clientID
     * @return PasswordAuthentication
     */
    public function clientId(string $clientID): PasswordAuthentication
    {
        $this->builder['client_id'] = $clientID;

        return $this;
    }

    /**
     * @param string $clientSecret
     * @return PasswordAuthentication
     */
    public function clientSecret(string $clientSecret): PasswordAuthentication
    {
        $this->builder['client_secret'] = $clientSecret;

        return $this;
    }

    /**
     * @param string $username
     * @return PasswordAuthentication
     */
    public function username(string $username): PasswordAuthentication
    {
        $this->builder['username'] = $username;

        return $this;
    }

    /**
     * @param string $password
     * @return PasswordAuthentication
     */
    public function password(string $password): PasswordAuthentication
    {
        $this->builder['password'] = $password;

        return $this;
    }

    /**
     * @return array
     */
    public function setBuilder(): array
    {
        return $this->builder;
    }
}
