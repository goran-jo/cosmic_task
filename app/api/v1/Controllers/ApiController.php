<?php

namespace App\Api\V1\Controllers;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param $response
     * @param int $code
     * @return JsonResponse
     */
    protected function response($response, int $code = 200): JsonResponse
    {
        return response()->json($response, $code);
    }

    /**
     * @param $message
     * @param $code
     * @param null $field
     * @return JsonResponse
     */
    protected function error($message, $code, $field = null): JsonResponse
    {
        $response = [
            'status' => 'error',
            'message' => $message
        ];

        if ($field) {
            $response['errorField'] = $field;
        }

        return $this->response($response, $code);
    }
}
