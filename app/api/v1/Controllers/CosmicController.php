<?php

namespace App\Api\V1\Controllers;

use App\Http\Resources\ApiResource;
use Illuminate\Http\Concerns\InteractsWithInput;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;


class CosmicController extends ApiController
{
    use InteractsWithInput;

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function search(Request $request): AnonymousResourceCollection
    {
        return ApiResource::collection($request);
    }
}
